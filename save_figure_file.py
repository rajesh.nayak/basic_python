import numpy as np
import matplotlib.pyplot as plt
## www.iiserkol.ac.in/~rajesh/my_python.html
font = {'family':'Tahoma',
	'color':  'C8',
	'weight': 'normal',
	'size': 18,
	}
font1 = {'family': 'Tahoma',
	'color':  'C9',
	'weight': 'normal',
	'size': 12,
	}
N=100
x=np.linspace(-10*np.pi,10*np.pi, N)
y1= np.sin(x)/x

fig= plt.figure(figsize=(11,7))
plt.plot(x, y1, 'C0o-', label='sinc(x)', mec='k', mfc='C1')
plt.title('sinc function', fontdict=font)
plt.legend()
plt.xlabel('x', fontdict=font1)
plt.ylabel('sinc', fontdict=font1)
plt.ylim([-0.4, 1.2])
plt.grid()
plt.savefig('plot_of_sinc') # png plot will be saved
plt.show()

