import numpy as np
import matplotlib.pyplot as plt
## www.iiserkol.ac.in/~rajesh/my_python.html
font = {'family':'Tahoma',
	'color':  'C8',
	'weight': 'normal',
	'size': 18,
	}
font1 = {'family': 'Tahoma',
	'color':  'C9',
	'weight': 'normal',
	'size': 12,
	}
N=100
x=np.linspace(-2*np.pi,2*np.pi, N)
y1= np.sin(x)
y2= np.cos(x)
y=y1*y1+y2*y2
fig= plt.figure(figsize=(11,7))
plt.plot(x, y1, 'C0-', label='sin(x)')
plt.plot(x, y2, 'C1-', label='cos(x)')
plt.plot(x, y, 'C2--', linewidth=3, label='sin^2(x)+cos^2(x)')
plt.title('Changing plot size', fontdict=font)
plt.legend()
plt.xlabel('x', fontdict=font1)
plt.ylabel('Value of functions', fontdict=font1)
plt.ylim([-1.2, 1.2])
plt.grid(alpha=0.5)
plt.show()

